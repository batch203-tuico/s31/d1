const http = require("http");

const port = 4000;

// Request and Response is an object, with property and methods.
const server = http.createServer((req, res) => {
    // We will create two endpoint route for "/greeting" and "/homepage" and will return a response upon accessing.
    // the "url" property refers to the url or the link in the browser (endpoint).
    // baseURI (/), is "/" == "/greeting" (false)
    if (req.url == "/greeting"){
        res.writeHead(200 , {"Content-Type": "text/plain"});
        res.end("Hello Again");
    }
    
        /*
            Mini Activity
    
            Create another endpoint for the "/homepage" and send a response "This is the homepage"
        
        */

    else if (req.url == "/homepage") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("This is the homepage");
    }
    else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Page not available");
    }

    
});

server.listen(port);

console.log(`Server is now accessible at localhost ${port}`);